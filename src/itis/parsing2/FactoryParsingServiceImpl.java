package itis.parsing2;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import itis.parsing2.FactoryParsingException.FactoryValidationError;
import itis.parsing2.annotations.NotBlank;


public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        Factory factory = new Factory();

        List<FactoryValidationError> validationErrors = new ArrayList<>();

        try {
            HashMap<String, String> dataFromFiles = readLinesFromDir(factoryDataDirectoryPath);

            for (Field field : factory.getClass().getDeclaredFields()) {

                String value = dataFromFiles.get(field.getName());
                if (value != null) {
                    setToValueToField(field, factory, value);
                }

                NotBlank notBlank = field.getAnnotation(NotBlank.class);
                if (notBlank != null) {
                    if (value == null) {
                        validationErrors.add(new FactoryValidationError(field.getName(), "Field is null"));
                    }
                }
            }
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (validationErrors.size() > 0) {
            throw new FactoryParsingException("Validation exception", validationErrors);
        }

        return factory;
    }


    private static void setToValueToField(Field field, Object obj, String value) throws IllegalAccessException {
        field.setAccessible(true);

        Class<?> fieldClass = field.getType();


        switch (fieldClass.getSimpleName()) {
            case "Long":
                field.set(obj, Integer.parseInt(value));
                break;
            case "String":
                field.set(obj, value);
                break;
            case "List":
                value = value.substring(1, value.length() - 1);
                String[] array = value.split(", ");
                field.set(obj, Arrays.asList(array));
                break;
        }
    }

    private static HashMap<String, String> readLinesFromDir(String pathToDir) throws IOException {

        HashMap<String, String> result = new HashMap<>();

        File[] filesList = new File(pathToDir).listFiles();
        for(File f : filesList){
            if(f.isFile()){

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                        new FileInputStream(f.getAbsolutePath())));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] splitLine = line.split(":", 2);

                    if (splitLine.length == 2) {
                        String key = splitLine[0].trim().replace("\"", "");
                        String value = splitLine[1].trim().replace("\"", "");

                        result.put(key, value);
                    }
                }

                bufferedReader.close();
            }
        }

        return result;
    }
}
